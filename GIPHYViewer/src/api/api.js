import axios from 'axios';
import Config from './config';
const gifRequest = axios.create({
    baseURL: `${Config.protocol}://${Config.ip}${Config.serv}`
})

export const apiGifSearch = (keyword) => gifRequest.get(`/search?q=${keyword}&api_key=${Config.apikey}`);
export const apiGifRequest = (offset) => gifRequest.get(`/trending?api_key=${Config.apikey}&limit=10&offset=${offset}`);
export const apiGifRequestByIds = (ids) => gifRequest.get('', {
    params: {
        api_key: Config.apikey,
        ids: ids
    }
  });