import React, { Component } from 'react';
import { View, ActivityIndicator, Text, Dimensions, TouchableWithoutFeedback, SafeAreaView, Alert} from 'react-native';
import { Icon, Container, Item, Input, Button } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import ImageLayout from "react-native-image-layout";
import styles from '../styles/generalStyle';
import { apiGifSearch, apiGifRequest } from '../api/api';
import Utils from '../utils/utils';
class SearchTab extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            searching: false,
            offset: 0,
            dataSource: [],
            imageSource: [],
            searchSource: [],
            totalcount: 0,
            count: 10,
            query: this.props.query || ""
        }
    }

    _onChangeSearchText = (text) => {
        this.setState({ query: text.trim().toLowerCase()});
        console.log(this.state.query);
    }

    getInitData = async () => {
        try {
            const all = await apiGifRequest(this.state.offset);
            console.log(all.data);
            if (all.data.pagination && all.data.data) {
                this.setState({ 
                    totalcount: all.data.pagination.total_count, 
                    count: all.data.pagination.count,
                    offset: all.data.pagination.offset,
                    dataSource: all.data.data,
                    imageSource: all.data.data.map((image) => {
                        var imgObj = {};
                        imgObj["image_id"] = image.id;
                        imgObj["uri"] = image.images.preview_gif.url;
                        imgObj["share"] = image.images.original.url;
                        return imgObj;
                    }),
                    isLoaded: true
                });
            }
            return all.data;
        } catch (error) {
            console.error(error);
        } finally {
            console.log(this.state.imageSource);
        }
    }

    loadMoreData = () => {
        this.setState({offset: this.state.offset + 1}, 
            async () => {
                if (this.state.dataSource.length < this.state.totalcount) {
                    try {
                        const all = await apiGifRequest(this.state.offset);
                        console.log(all.data);
                        if (all.data.pagination && all.data.data) {
                            this.setState({ 
                                totalcount: all.data.pagination.total_count, 
                                count: all.data.pagination.count,
                                offset: all.data.pagination.offset,
                                dataSource: Utils.getUnique([...this.state.dataSource, ...all.data.data], 'id'),
                                imageSource: Utils.getUnique([...this.state.imageSource, ...all.data.data.map((image) => {
                                    var imgObj = {};
                                    imgObj["image_id"] = image.id;
                                    imgObj["uri"] = image.images.preview_gif.url;
                                    imgObj["share"] = image.images.original.url;
                                    return imgObj;
                                })], 'image_id'),
                                isLoaded: true
                            });
                        }
                        return all.data;
                    } catch (error) {
                        console.error(error);
                    }
                } else {
                    console.log("All Images are loaded.");
                }
            }
        );
    }

    addToFavouriteList = async (image) => {
        try {
            const value = await AsyncStorage.getItem('@Favourite_Ids');
            if (value !== null) {
                console.log(image.image_id);
                if (value.includes(image.image_id)) {
                    Alert.alert("This image has been added already!");
                } else {
                    await AsyncStorage.setItem('@Favourite_Ids', `${value},${image.image_id}`);
                    Alert.alert("This GIF is added to favourite list.");
                }
            } else {
                await AsyncStorage.setItem('@Favourite_Ids', `${image.image_id}`);
                Alert.alert("This GIF is added to favourite list.");
            }
        } catch(e) {
            console.log(e);
        }
    }

    componentWillMount(){
        this.getInitData();
    }

    handleSearch = () => {
        this.setState({ isLoaded: false }, async () => {
            try {
                const searchData = await apiGifSearch(this.state.query);
                console.log(searchData.data);
                if (searchData.data.pagination && searchData.data.data) {
                    this.setState({ 
                        dataSource: searchData.data.data,
                        searchSource: searchData.data.data.map((image) => {
                            var imgObj = {};
                            imgObj["image_id"] = image.id;
                            imgObj["uri"] = image.images.preview_gif.url;
                            imgObj["share"] = image.images.original.url;
                            return imgObj;
                        }),
                        isLoaded: true,
                        searching: true
                    });
                }
                return searchData.data;
            } catch (error) {
                console.error(error);
            }
        });
    }

    cancelSearch = () => {
        if (this.input != null) {
            this.input._root.clear();
        }
        this.setState({ searching: false, query: "", isLoaded: false, offset: 0}, this.getInitData);
    }

    render() {
        return (
            (this.state.isLoaded) ?
                <Container>
                    <Item style={{width: '100%'}}>
                        <Item style={styles.itemContainer}>
                            <Icon name="ios-search" style={styles.iconSearchBar}/>
                            <Input style={styles.inputSearchBar} ref={(ref) => { this.input = ref }} placeholder="Type keyword(s) to search..." onChangeText={this._onChangeSearchText.bind(this)} />
                        </Item>
                        <Button transparent style={styles.buttonSearchBar} onPress={(this.state.searching) ? this.cancelSearch : this.handleSearch}>
                            {(this.state.searching) ? <Text style={styles.textSearchBar}>Cancel</Text> : <Text style={styles.textSearchBar}>Search</Text>}
                        </Button>
                    </Item>
                    {(this.state.searching && this.state.searchSource) ? 
                        (this.state.searchSource.length > 0) ?
                            <ImageLayout
                                spacing={2}
                                renderPageHeader={this._renderPageHeader}
                                renderPageFooter={this._renderPageFooter}
                                errorPageComponent={this._errorPageComponent}
                                images={this.state.searchSource}
                            />
                        :
                            <View style={styles.fillContainerCentral}>
                                <Text>Cannot find any Images with "{this.state.query}"</Text>
                            </View>
                    :
                        <ImageLayout
                            spacing={2}
                            renderPageHeader={this._renderPageHeader}
                            renderPageFooter={this._renderPageFooter}
                            errorPageComponent={this._errorPageComponent}
                            images={this.state.imageSource}
                            masonryFlatListColProps={{
                                onEndReached: this.loadMoreData,
                                onEndReachedThreshold: 1
                            }}
                        />
                    }
                </Container>
            :
                <View style={[styles.fillContainerCentral, {marginTop: Dimensions.get('window').height / 2 - 150}]}>
                    <ActivityIndicator size="large"/>
                    <Text>Images are fetching...</Text>
                </View>
        )
    }
    
    _renderPageHeader = (image, index, onClose) => {
        return (
            <View style={{flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                <TouchableWithoutFeedback onPress={() => {onClose()}}>
                    <Icon name="close" style={{marginTop: 25, marginRight: 25, fontSize: 70, color: '#FFFFFF'}}/>
                </TouchableWithoutFeedback>
            </View>
        );
    }

    _renderPageFooter = (image, index, onClose) => {
        // Individual image object data.
        console.log(image);
        return (
            <SafeAreaView style={{flex: 1, alignItems: 'flex-start', justifyContent: 'space-evenly', flexDirection: 'row'}}>
                <TouchableWithoutFeedback onPress={() => {this.addToFavouriteList(image)}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Icon name="star-outline" style={{marginRight: 5, fontSize: 25, color: '#FFFFFF'}}/>
                        <Text style={{ color: '#FFFFFF', fontSize: 12}}>Add to Favourite List</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {Utils.shareGif(image)}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Icon name="share" style={{marginRight: 10, fontSize: 25, color: '#FFFFFF'}}/>
                        <Text style={{ color: '#FFFFFF', fontSize: 12}}>Share to Friends</Text>
                    </View>
                </TouchableWithoutFeedback>
            </SafeAreaView>
        );
    }

    _errorPageComponent = () => 
        <View style={[styles.fillContainerCentral, {marginTop: Dimensions.get('window').height / 2 - 150}]}>
            <Text>Image cannot be loaded.</Text>
        </View>
}
export default SearchTab;