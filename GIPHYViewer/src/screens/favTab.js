import React, { Component } from 'react';
import { View, ActivityIndicator, Text, Dimensions, RefreshControl, TouchableWithoutFeedback, SafeAreaView, ScrollView} from 'react-native';
import { Icon } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import ImageLayout from "react-native-image-layout";
import styles from '../styles/generalStyle';
import { apiGifRequestByIds } from '../api/api';
import Utils from '../utils/utils';
class FavTab extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoaded: false,
            refreshing: false,
            dataSource: [],
            imageSource: [],
            ids: ""
        }
    }

    componentWillMount(){
        this.getFavouriteGIFs();
    }

    _onRefresh = () => {
        this.setState({refreshing: true});
        this.getFavouriteGIFs().then(() => {
          this.setState({refreshing: false});
        });
    }

    removeFromFavouriteList = async (image, onClose) => {
        for( var i = 0; i < this.state.imageSource.length; i++){ 
            if ( this.state.imageSource[i].image_id === image.image_id) {
                this.state.imageSource.splice(i, 1); 
            }
        }
        try {
            console.log(this.state.ids, image.image_id);
            let tempArray = this.state.ids.split(",").filter((id) => id != image.image_id);
            console.log(tempArray);
            await AsyncStorage.setItem('@Favourite_Ids', (tempArray.length > 1) ? tempArray.join(",") : tempArray.toString());
            this.setState({ isLoaded: false }, this.getFavouriteGIFs);
        } catch(e) {
            console.log(e);
        } finally{
            onClose();
        }
    }

    getFavouriteGIFs = async () => {
        try {
            //await AsyncStorage.removeItem('@Favourite_Ids');
            const value = await AsyncStorage.getItem('@Favourite_Ids');
            if (value !== null) {
                try {
                    console.log(value);
                    const all_data = await apiGifRequestByIds(value);
                    console.log(all_data.data);
                    if (all_data.data.pagination && all_data.data.data) {
                        this.setState({ 
                            dataSource: all_data.data.data,
                            imageSource: all_data.data.data.map((image) => {
                                var imgObj = {};
                                imgObj["image_id"] = image.id;
                                imgObj["uri"] = image.images.preview_gif.url;
                                imgObj["share"] = image.images.original.url;
                                return imgObj;
                            }),
                            isLoaded: true,
                            ids: value
                        });
                    }
                    return all_data.data;
                } catch (error) {
                    console.error(error);
                }
            } else {
                this.setState({ isLoaded: true });
            }
        } catch(e) {
            console.error(e);
        }
    }

    render() {
        return (
            (this.state.isLoaded) ?
                (this.state.dataSource.length > 0) ?
                    <ImageLayout
                        spacing={2}
                        rerender={true}
                        renderPageHeader={this._renderPageHeader}
                        renderPageFooter={this._renderPageFooter}
                        errorPageComponent={this._errorPageComponent}
                        images={this.state.imageSource}
                        masonryFlatListColProps={{
                            refreshControl: 
                                <RefreshControl
                                    refreshing={this.state.refreshing}
                                    onRefresh={this._onRefresh}
                                />
                        }}
                    />
                :
                    <ScrollView 
                        style={{flex: 1}} 
                        contentContainerStyle={{alignItems: 'center', justifyContent: 'center'}}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                    >
                        <Text style={{marginTop: Dimensions.get('window').height / 2 - 150}}>You have no favourite GIF.</Text>
                    </ScrollView>
            :
                <View style={styles.fillContainerCentral}>
                    <ActivityIndicator size="large"/>
                    <Text>Images are fetching...</Text>
                </View>
        )
    }

    _renderPageHeader = (image, index, onClose) => {
        // Individual image object data.
        console.log(image);
        return (
            <View style={{flex: 1, alignItems: 'flex-end', justifyContent: 'flex-end'}}>
                <TouchableWithoutFeedback onPress={() => {onClose()}}>
                    <Icon name="close" style={{marginTop: 25, marginRight: 25, fontSize: 70, color: '#FFFFFF'}}/>
                </TouchableWithoutFeedback>
            </View>
        );
    }

    _renderPageFooter = (image, index, onClose) => {
        // Individual image object data.
        console.log(image);
        return (
            <SafeAreaView style={{flex: 1, alignItems: 'flex-start', justifyContent: 'space-evenly', flexDirection: 'row'}}>
                <TouchableWithoutFeedback onPress={() => {this.removeFromFavouriteList(image, onClose)}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Icon name="star" style={{marginRight: 5, fontSize: 25, color: '#FFFFFF'}}/>
                        <Text style={{ color: '#FFFFFF', fontSize: 12}}>Remove from Favourite List</Text>
                    </View>
                </TouchableWithoutFeedback>
                <TouchableWithoutFeedback onPress={() => {Utils.shareGif(image)}}>
                    <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}}>
                        <Icon name="share" style={{marginRight: 10, fontSize: 25, color: '#FFFFFF'}}/>
                        <Text style={{ color: '#FFFFFF', fontSize: 12}}>Share to Friends</Text>
                    </View>
                </TouchableWithoutFeedback>
            </SafeAreaView>
        );
    }

    _errorPageComponent = () => 
        <View style={[styles.fillContainerCentral, {marginTop: Dimensions.get('window').height / 2 - 150}]}>
            <Text>Image cannot be loaded.</Text>
        </View>
}
export default FavTab;