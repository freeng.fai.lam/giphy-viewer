import React, { Component } from 'react';
import { SafeAreaView, Text } from 'react-native';
import { Container, Content, Tabs, Tab, TabHeading, Icon, Header, Body, Title } from 'native-base';
import SeacrchTab from './searchTab';
import FavTab from './favTab';
import styles from '../styles/generalStyle';

class SearchScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
          currentTab: 0
        }
    }

    render() {
        return (
            <Container style={styles.fillContainerCentral}>
                <Header style={{width: '100%'}}>
                    <Body>
                        <Title>GIPHY Viewer</Title>
                    </Body>
                </Header>
                <Content>
                    <SafeAreaView style={styles.fillContainerCentral}>
                        <Tabs onChangeTab={({i}) => this.setState({ currentTab: i })} tabBarPosition="top" >
                            <Tab heading={<TabHeading><Icon name="search" style={{ marginRight: 5 }}/><Text>Search Page</Text></TabHeading>}>
                                <SeacrchTab/>
                            </Tab>
                            <Tab heading={<TabHeading><Icon name="star" style={{ marginRight: 5 }}/><Text>My Favourite Page</Text></TabHeading>}>
                                <FavTab/>
                            </Tab>
                        </Tabs>
                    </SafeAreaView>
                </Content>
            </Container>
        );
    }
}
export default SearchScreen;