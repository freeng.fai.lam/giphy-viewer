import { Share } from 'react-native';
/**
 * Filter the array list by object key.
 *
 * @function getUnique
 */
const getUnique = (arr, key) => arr.map(e => e[key])
    .map((e, i, final) => final.indexOf(e) === i && i)
    .filter(e => arr[e]).map(e => arr[e]);

/**
 * Share Gif via default Share Action.
 *
 * @function shareGif
 */
const shareGif = (image) => {
        console.log(image);
        Share.share({
            message: image.share,
            url: image.share
        }, {
            excludedActivityTypes: [
            'com.apple.UIKit.activity.PostToTwitter'
            ],
            tintColor: 'green'
        })
        .then(this._showResult)
        .catch((error) => this.setState({result: 'error: ' + error.message}));
    }

/**
 * Handle the result of shareGif.
 *
 * @function _showResult
 */
    _showResult = (result) => {
        if (result.action === Share.sharedAction) {
        if (result.activityType) {
            Alert.alert("This GIF is shared with " + result.activityType + ".");
        } else {
            Alert.alert("This GIF is shared.");
        }
        } else if (result.action === Share.dismissedAction) {
            console.log('Dismissed');
        }
        console.log(result);
    }

export default
{
    getUnique, shareGif
}
