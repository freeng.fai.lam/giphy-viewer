import { StyleSheet, Platform } from 'react-native';
export default StyleSheet.create({
    fillContainerCentral: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemContainer: {
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 0,
        flex: 1,
        height: (Platform.OS == "ios") ? 30 : 40,
        borderColor: 'transparent',
        backgroundColor: (Platform.OS == "ios") ? '#CECDD2' : '#fff',
        borderRadius: (Platform.OS == "ios") ? 25 : 3
    },
    iconSearchBar: {
        backgroundColor: 'transparent',
        color: '#414142',
        fontSize: (Platform.OS == "ios") ? 20 : 23,
        alignItems: 'center',
        marginTop: 2,
        paddingRight: 10,
        paddingLeft: 10
    },
    inputSearchBar: {
        alignSelf: 'center',
        lineHeight: null,
        height: (Platform.OS == "ios") ? 30 : 50,
        fontSize: 12
    },
    buttonSearchBar: {
        paddingHorizontal: (Platform.OS == "ios") ? undefined : null,
        width: (Platform.OS == "ios") ? undefined : 0,
        height: (Platform.OS == "ios") ? undefined : 0
    },
    textSearchBar: {
        fontWeight: '500',
        paddingHorizontal: 10,
        //paddingLeft: (Platform.OS == "ios") ? 5 : null,
    }
});