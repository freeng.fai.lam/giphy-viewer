/**
 * GIPHY Viewer
 * 
 * 
 * @author Caden Lam
 * 
 */
import React from 'react';
import { View, StatusBar } from 'react-native';
import SearchScreen from './src/screens/searchScreen';
import styles from './src/styles/generalStyle';
const App = () => {
  return (
    <View style={styles.fillContainerCentral}>
      	<StatusBar barStyle="dark-content" />
		    <SearchScreen/>
    </View>
  );
};

export default App;
